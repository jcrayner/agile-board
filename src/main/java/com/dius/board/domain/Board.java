package com.dius.board.domain;

import java.util.Objects;

/**
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class Board {
  private final Iteration iteration;

  public Board(final Iteration iteration) {
    this.iteration = iteration;
  }

  public Iteration getIteration() {
    return iteration;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Board board = (Board) o;
    return Objects.equals(iteration, board.iteration);
  }

  @Override
  public int hashCode() {
    return Objects.hash(iteration);
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("Board{");
    sb.append("iteration=").append(iteration);
    sb.append('}');
    return sb.toString();
  }
}
