package com.dius.board.domain;

import java.util.Objects;

/**
 * Card model. Has a title, a description and a story point estimate
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class Card {
  private final String title;
  private final String description;
  private final int storyPoints;

  public Card(final String title, final String description, final int storyPoints) {
    if(storyPoints <= 0) {
      throw new IllegalArgumentException("Story points must be a value > 0");
    }

    this.title = title;
    this.description = description;
    this.storyPoints = storyPoints;
  }

  public String getTitle() {
    return title;
  }

  public String getDescription() {
    return description;
  }

  public int getStoryPoints() {
    return storyPoints;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Card card = (Card) o;
    return Objects.equals(title, card.title) &&
        Objects.equals(description, card.description) &&
        Objects.equals(storyPoints, card.storyPoints);
  }

  @Override
  public int hashCode() {
    return Objects.hash(title, description, storyPoints);
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("Card{");
    sb.append("title='").append(title).append('\'');
    sb.append(", description='").append(description).append('\'');
    sb.append(", storyPoints=").append(storyPoints);
    sb.append('}');
    return sb.toString();
  }
}
