package com.dius.board.domain;

import java.util.Objects;

/**
 * Column model. Has a name, a state and an optional story point limit (will default to {@code Integer.MAX_VALUE})
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class Column {
  private final String name;
  private final ColumnState state;
  private final Integer storyPointLimit;

  public Column(final String name, final ColumnState state) {
    this(name, state, Integer.MAX_VALUE);
  }

  public Column(final String name, final ColumnState state, final int storyPointLimit) {
    if(storyPointLimit <= 0) {
      throw new IllegalArgumentException("Story point limit must be greater than 0");
    }

    this.name = name;
    this.state = state;
    this.storyPointLimit = storyPointLimit;
  }

  public String getName() {
    return name;
  }

  public ColumnState getState() {
    return state;
  }

  public Integer getStoryPointLimit() {
    return storyPointLimit;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Column column = (Column) o;
    return Objects.equals(name, column.name) &&
        state == column.state &&
        Objects.equals(storyPointLimit, column.storyPointLimit);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, state, storyPointLimit);
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("Column{");
    sb.append("name='").append(name).append('\'');
    sb.append(", state=").append(state);
    sb.append(", storyPointLimit=").append(storyPointLimit);
    sb.append('}');
    return sb.toString();
  }
}
