package com.dius.board.domain;

/**
 * Exception for when an attempt to associate a card to a column within an iteration results in a breach of work limits
 * (story point upper threshold).
 *
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class ColumnLimitBreachedException extends Exception {
  public ColumnLimitBreachedException(final Card card, final Column column, final Integer attemptedPointAggregate) {
    super(
        "Adding card " + card.getTitle() + " to column " + column.getName() + " results in a work commitment of " + attemptedPointAggregate +
            ", which breaches the current limit of " + column.getStoryPointLimit() + " within the current iteration"
    );
  }
}
