package com.dius.board.domain;

/**
 * Basic column state model
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public enum ColumnState {
  BACKLOG, // start state
  WIP,     // in-progress state
  DONE     // done state
}
