package com.dius.board.domain;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * Exception which represents an attempt to query a column which does not exist within an iteration.
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class InvalidColumnException extends Exception {
  public InvalidColumnException(Column invalidColumn, Set<Column> validColumns) {
    super(
        "Provided column " + invalidColumn + " is not a valid column. Valid columns within the iteration are "
        + validColumns.stream().map(Column::toString).collect(Collectors.joining(", "))
    );
  }
}
