package com.dius.board.domain;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class Iteration {
  private static final Predicate<Column> START_COLUMN = c -> c.getState().equals(ColumnState.BACKLOG);
  private static final Predicate<Column> DONE_COLUMN  = c -> c.getState().equals(ColumnState.DONE);

  private Set<CardWithinIteration> cards;
  private final Set<Column> columns;

  private final Column startColumn;
  private final Column doneColumn;

  private CardMoveEvent lastMove = null;

  public Iteration(final Set<Column> columns) {
    this.startColumn = columns.stream().filter(START_COLUMN).findFirst()
        .orElseThrow(() -> new IllegalArgumentException("Provided column set does not contain a start column"));

    this.doneColumn = columns.stream().filter(DONE_COLUMN).findFirst()
        .orElseThrow(() -> new IllegalArgumentException("Provided column set does not contain a done column"));

    this.cards = new HashSet<>();
    this.columns = columns;
  }

  /**
   * Add a {@code Card} to this iteration.
   * @param card    A card
   *
   * @throws InvalidColumnException         thrown when the requested column does not exist within the iteration
   * @throws ColumnLimitBreachedException   thrown when the destination column has a story point threshold which would be
   *                                        breached if this card was moved within it
   */
  public void add(Card card) throws InvalidColumnException, ColumnLimitBreachedException {
    checkIfCardBreachesColumnLimit(card, startColumn);
    cards.add(new CardWithinIteration(card, startColumn));
  }

  /**
   * Calculate the velocity of this iteration (sum of card story points within the column with a {@code ColumnState}
   * of {@code ColumnState.DONE})
   *
   * @return  An integer
   */
  public Integer velocity() {
    return cards.stream().filter(withinColumn(doneColumn))
        .mapToInt(wrapper -> wrapper.getCard().getStoryPoints())
        .sum();
  }

  /**
   * Fetch all the cards within a given column
   * @param column    The target column
   * @return          A {@code Set} of {@code Card}
   *
   * @throws InvalidColumnException   thrown when the requested column does not exist within the iteration
   */
  public Set<Card> getCardsInColumn(final Column column) throws InvalidColumnException {
    checkIfColumnIsPresent(column);

    return cards.stream().filter(withinColumn(column))
        .map(CardWithinIteration::getCard)
        .collect(Collectors.toSet());
  }

  /**
   * Move a card from one distinct column to another
   * @param card    A {@code Card}
   * @param column  The destination {@code Column}
   *
   * @throws InvalidColumnException         thrown when the requested column does not exist within the iteration
   * @throws ColumnLimitBreachedException   thrown when the destination column has a story point threshold which would be
   *                                        breached if this card was moved within it
   */
  public void moveCard(final Card card, final Column column) throws InvalidColumnException, ColumnLimitBreachedException {
    checkIfColumnIsPresent(column);
    checkIfCardBreachesColumnLimit(card, column);

    lastMove = findCard(card).filter(withinColumn(column).negate())
        .map(wrapper -> moveCard(wrapper, column))
        .orElse(null);
  }

  /**
   * Undo the last card move within an iteration (if it exists)
   */
  public void undoLastMove() {
    Optional.ofNullable(lastMove).ifPresent(this::rollbackEvent);
    lastMove = null;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("Iteration{");
    sb.append("cards=").append(cards);
    sb.append(", columns=").append(columns);
    sb.append('}');
    return sb.toString();
  }

  private Optional<CardWithinIteration> findCard(final Card card) {
    return cards.stream().filter(matchesCard(card)).findFirst();
  }

  private CardMoveEvent moveCard(CardWithinIteration wrapper, final Column column) {
    CardMoveEvent event = new CardMoveEvent(wrapper.getCard(), wrapper.getColumn(), column);
    wrapper.setColumn(column);
    return event;
  }

  private void rollbackEvent(final CardMoveEvent event) {
    cards.stream().filter(matchesCard(event.getCard())).forEach((wrapper -> wrapper.setColumn(event.getFromColumn())));
  }

  private void checkIfColumnIsPresent(final Column column) throws InvalidColumnException {
    if(!columns.contains(column)) {
      throw new InvalidColumnException(column, columns);
    }
  }

  private void checkIfCardBreachesColumnLimit(Card card, Column column) throws InvalidColumnException, ColumnLimitBreachedException {
    final Integer newSize = getCardsInColumn(startColumn).stream().mapToInt(Card::getStoryPoints).sum() + card.getStoryPoints();

    if(newSize > column.getStoryPointLimit()) {
      throw new ColumnLimitBreachedException(card, column, newSize);
    }
  }

  private static Predicate<CardWithinIteration> matchesCard(final Card card) {
    return wrapper -> wrapper.getCard().equals(card);
  }

  private static Predicate<CardWithinIteration> withinColumn(final Column column) {
    return wrapper -> wrapper.getColumn().equals(column);
  }

  /**
   * Event model representing the transition of a card's position from a column to another column within
   * an iteration.
   */
  static class CardMoveEvent {
    private final Card card;
    private final Column fromColumn;
    private final Column toColumn;

    public CardMoveEvent(Card card, Column fromColumn, Column toColumn) {
      this.card = card;
      this.fromColumn = fromColumn;
      this.toColumn = toColumn;
    }

    public Card getCard() {
      return card;
    }

    public Column getFromColumn() {
      return fromColumn;
    }

    public Column getToColumn() {
      return toColumn;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      CardMoveEvent that = (CardMoveEvent) o;
      return Objects.equals(card, that.card) &&
          Objects.equals(fromColumn, that.fromColumn) &&
          Objects.equals(toColumn, that.toColumn);
    }

    @Override
    public int hashCode() {
      return Objects.hash(card, fromColumn, toColumn);
    }

    @Override
    public String toString() {
      final StringBuilder sb = new StringBuilder("CardMoveEvent{");
      sb.append("card=").append(card);
      sb.append(", fromColumn=").append(fromColumn);
      sb.append(", toColumn=").append(toColumn);
      sb.append('}');
      return sb.toString();
    }
  }

  /**
   * Model representing a card within an iteration.
   */
  static class CardWithinIteration {
    final Card card;
    Column column;

    CardWithinIteration(final Card card, Column column) {
      this.card = card;
      this.column = column;
    }

    Card getCard() {
      return card;
    }

    Column getColumn() {
      return column;
    }

    void setColumn(Column column) {
      this.column = column;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      CardWithinIteration that = (CardWithinIteration) o;
      return Objects.equals(card, that.card) &&
          Objects.equals(column, that.column);
    }

    @Override
    public int hashCode() {
      return Objects.hash(card, column);
    }

    @Override
    public String toString() {
      final StringBuilder sb = new StringBuilder("CardWithinIteration{");
      sb.append("card=").append(card);
      sb.append(", column=").append(column);
      sb.append('}');
      return sb.toString();
    }
  }
}
