package com.dius.board;

import com.dius.board.domain.Board;
import com.dius.board.domain.Card;
import com.dius.board.domain.Column;
import com.dius.board.domain.ColumnLimitBreachedException;
import com.dius.board.domain.ColumnState;
import com.dius.board.domain.InvalidColumnException;
import com.dius.board.domain.Iteration;
import org.hamcrest.Matchers;
import org.hamcrest.collection.IsIterableContainingInOrder;
import org.hamcrest.collection.IsIterableWithSize;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.LinkedHashSet;
import java.util.Set;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class AgileBoardTests {
  @Rule
  public ExpectedException expectedException = ExpectedException.none();

  @Test
  public void testCalculationOfVelocity() throws Exception {
    Column backlog = new Column("Backlog", ColumnState.BACKLOG);
    Column wip     = new Column("Work in Progress", ColumnState.WIP);
    Column done    = new Column("Done", ColumnState.DONE);

    Set<Column> columns = new LinkedHashSet<>(3);
    columns.add(backlog);
    columns.add(wip);
    columns.add(done);

    Board board = new Board(new Iteration(columns));

    Card card1 = new Card("Test velocity", "Test velocity of an iteration", 1);
    board.getIteration().add(card1);
    Card card2 = new Card("Score job", "Get l33t job due to sweet haxxorz", 20);
    board.getIteration().add(card2);
    board.getIteration().moveCard(card2, done);

    assertThat(board.getIteration().velocity(), is(equalTo(20)));
  }

  @Test
  public void testTransitionOfCardState() throws Exception {
    Column backlog = new Column("Backlog", ColumnState.BACKLOG);
    Column wip     = new Column("Work in Progress", ColumnState.WIP);
    Column done    = new Column("Done", ColumnState.DONE);

    Set<Column> columns = new LinkedHashSet<>(3);
    columns.add(backlog);
    columns.add(wip);
    columns.add(done);

    Board board = new Board(new Iteration(columns));

    Card card1 = new Card("Test velocity", "Test velocity of an iteration", 1);
    board.getIteration().add(card1);
    board.getIteration().moveCard(card1, wip);

    assertThat(
        board.getIteration().getCardsInColumn(wip),
        Matchers.allOf(
            IsIterableWithSize.<Card>iterableWithSize(equalTo(1)),
            IsIterableContainingInOrder.<Card>contains(equalTo(card1))
        )
    );
  }

  @Test
  public void testUndoOfLastMove() throws Exception {
    Column backlog = new Column("Backlog", ColumnState.BACKLOG);
    Column wip     = new Column("Work in Progress", ColumnState.WIP);
    Column done    = new Column("Done", ColumnState.DONE);

    Set<Column> columns = new LinkedHashSet<>(3);
    columns.add(backlog);
    columns.add(wip);
    columns.add(done);

    Board board = new Board(new Iteration(columns));

    Card card1 = new Card("Test velocity", "Test velocity of an iteration", 1);
    board.getIteration().add(card1);
    board.getIteration().moveCard(card1, wip);
    board.getIteration().undoLastMove();

    assertThat(
        board.getIteration().getCardsInColumn(backlog),
        Matchers.allOf(
            IsIterableWithSize.<Card>iterableWithSize(equalTo(1)),
            IsIterableContainingInOrder.<Card>contains(equalTo(card1))
        )
    );
  }

  @Test
  public void testUndoWhenNoLastMoveExists() throws Exception {
    Column backlog = new Column("Backlog", ColumnState.BACKLOG);
    Column wip     = new Column("Work in Progress", ColumnState.WIP);
    Column done    = new Column("Done", ColumnState.DONE);

    Set<Column> columns = new LinkedHashSet<>(3);
    columns.add(backlog);
    columns.add(wip);
    columns.add(done);

    Board board = new Board(new Iteration(columns));

    Card card1 = new Card("Test velocity", "Test velocity of an iteration", 1);
    board.getIteration().add(card1);

    // validate state
    assertThat(
        board.getIteration().getCardsInColumn(backlog),
        Matchers.allOf(
            IsIterableWithSize.<Card>iterableWithSize(equalTo(1)),
            IsIterableContainingInOrder.<Card>contains(equalTo(card1))
        )
    );

    board.getIteration().undoLastMove();

    // undo has no effect as no last move exists
    assertThat(
        board.getIteration().getCardsInColumn(backlog),
        Matchers.allOf(
            IsIterableWithSize.<Card>iterableWithSize(equalTo(1)),
            IsIterableContainingInOrder.<Card>contains(equalTo(card1))
        )
    );
  }

  @Test
  public void testMoveOfCardIntoColumnWhichDoesNotExistInIteration() throws Exception {
    expectedException.expect(InvalidColumnException.class);

    Column backlog = new Column("Backlog", ColumnState.BACKLOG);
    Column wip     = new Column("Work in Progress", ColumnState.WIP);
    Column done    = new Column("Done", ColumnState.DONE);

    Set<Column> columns = new LinkedHashSet<>(2);
    columns.add(backlog);
    columns.add(done);

    Board board = new Board(new Iteration(columns));

    Card card1 = new Card("Test velocity", "Test velocity of an iteration", 1);
    board.getIteration().add(card1);
    board.getIteration().moveCard(card1, wip);
  }

  @Test
  public void testAttemptToGetCardsForColumnWhichDoesNotExistInIteration() throws Exception {
    expectedException.expect(InvalidColumnException.class);
    Column backlog = new Column("Backlog", ColumnState.BACKLOG);
    Column wip     = new Column("Work in Progress", ColumnState.WIP);
    Column done    = new Column("Done", ColumnState.DONE);

    Set<Column> columns = new LinkedHashSet<>(2);
    columns.add(backlog);
    columns.add(done);

    Board board = new Board(new Iteration(columns));

    Card card1 = new Card("Test velocity", "Test velocity of an iteration", 1);
    board.getIteration().add(card1);
    board.getIteration().getCardsInColumn(wip);
  }

  @Test
  public void testAddingANewCardBreachesColumnLimit() throws Exception {
    expectedException.expect(ColumnLimitBreachedException.class);

    Column backlog = new Column("Backlog", ColumnState.BACKLOG, 1);
    Column wip     = new Column("Work in Progress", ColumnState.WIP);
    Column done    = new Column("Done", ColumnState.DONE);

    Set<Column> columns = new LinkedHashSet<>(3);
    columns.add(backlog);
    columns.add(wip);
    columns.add(done);

    Board board = new Board(new Iteration(columns));
    Card card1 = new Card("Test velocity", "Test velocity of an iteration", 20);
    board.getIteration().add(card1);
  }

  @Test
  public void testMovingACardBreachesColumnLimit() throws Exception {
    expectedException.expect(ColumnLimitBreachedException.class);

    Column backlog = new Column("Backlog", ColumnState.BACKLOG);
    Column wip     = new Column("Work in Progress", ColumnState.WIP, 1);
    Column done    = new Column("Done", ColumnState.DONE);

    Set<Column> columns = new LinkedHashSet<>(3);
    columns.add(backlog);
    columns.add(wip);
    columns.add(done);

    Board board = new Board(new Iteration(columns));
    Card card1 = new Card("Test velocity", "Test velocity of an iteration", 20);
    board.getIteration().add(card1);
    board.getIteration().moveCard(card1, wip);
  }
}
