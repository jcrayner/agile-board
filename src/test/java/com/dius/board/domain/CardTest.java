package com.dius.board.domain;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class CardTest {
  @Rule
  public ExpectedException expectedException = ExpectedException.none();

  @Test
  public void testConstructWithInvalidStoryPointValue() throws Exception {
    expectedException.expect(IllegalArgumentException.class);
    new Card("title", "description", 0);
  }
}