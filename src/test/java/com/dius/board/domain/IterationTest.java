package com.dius.board.domain;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class IterationTest {
  @Rule
  public ExpectedException expectedException = ExpectedException.none();

  @Test
  public void testConstructWithMissingStartColumn() throws Exception {
    expectedException.expect(IllegalArgumentException.class);
    new Iteration(Collections.emptySet());
  }

  @Test
  public void testConstructWithMissingDoneColumn() throws Exception {
    expectedException.expect(IllegalArgumentException.class);
    Column backlog = new Column("Backlog", ColumnState.BACKLOG);

    Set<Column> columns = new LinkedHashSet<>(2);
    columns.add(backlog);
    new Iteration(columns);
  }
}